// slick slider
$('.carousel').slick({
    infinite:false,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay:true,
    autoplaySpeed: 3000,
    speed:500,
    prevArrow:"<button type='button' class='slick-custom-previous-arrow'></button>",
    nextArrow:"<button type='button' class='slick-custom-next-arrow'></button>",

});

